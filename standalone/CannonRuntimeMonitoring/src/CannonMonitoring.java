import java.io.File;
import java.text.ParseException;
import org.jdom.input.SAXBuilder;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.Element;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author mattoop
 */
public class CannonMonitoring {

    public static void main(String[] args) {
        CannonMonitoring cannonMonitoring  = new CannonMonitoring();
        String currentTimestamp = cannonMonitoring.parseCurrentTimestamp("C:\\CanonParameterUpdater\\bin\\out.xml");
        long diffHours = cannonMonitoring.calculateTimestampDiff(currentTimestamp);
        if(diffHours >12) {
            cannonMonitoring.sendAlertEmail(diffHours);
        }
    }

    public String parseCurrentTimestamp(String filePath) {
        String content=null;
        SAXBuilder builder = new SAXBuilder();
        try {
            File cannonRuntimeFile = new File(filePath);

            Document document = builder.build(cannonRuntimeFile);
            Element root = document.getRootElement();
            Element paragraph = root.getChild("current_timestamp");

            content = paragraph.getText();
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
         return content;
    }

    public long calculateTimestampDiff(String cannonTimestamp) {
        DateFormat dateFormat  = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
        Date cannonDate = null;
        Date today = new Date();
        try {
            cannonDate = dateFormat.parse(cannonTimestamp);
        } catch (ParseException ex) {
            System.err.println(ex.getMessage());
        }
        return diffBtnTimestamps(today, cannonDate);
    }

    private long diffBtnTimestamps(Date dateOne, Date dateTwo) {
        return (dateOne.getTime()-dateTwo.getTime())/(1000*3600);
    }

    private void sendAlertEmail(long diffHours) {
    try {
            String host = "owa.skyworksinc.com";
            String from = "cannon_monitoring@skyworksinc.com";
            /** modified by Chen Chen 1/20/2014 **/
            //String to[] = {"nirav.thakkar@skyworksinc.com", "doug.beasley@skyworksinc.com", "piyush.mattoo@skyworksinc.com"};
            String to[] = {"chen.chen@skyworksinc.com", "Kevin.Duraj@skyworksinc.com", "piyush.mattoo@skyworksinc.com"};
            /** modified by Chen Chen 1/20/2014 end **/
            
            String subject = "Cannon Runtime not updated in a while";
            String message = "Difference between last updated timestamp and now is "+diffHours;

            Properties prop = System.getProperties();
            prop.put("mail.smtp.host", host);
            Session ses1 = Session.getDefaultInstance(prop, null);
            MimeMessage msg = new MimeMessage(ses1);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, toAddress);
            msg.setSubject(subject);

            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText(message);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);

            msg.setContent(multipart);
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}
